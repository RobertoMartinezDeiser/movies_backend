## Desplegar en NAS Synology

1. Eliminar los posibles contenedores/imagenes anteriores en docker (esto se puede hacer facilmente desde DSM).
2. Entrar por SSH al NAS.
3. Ejecutar el comando `sudo -i` para obtener permisos root.
3. Ir a la carpeta del proyecto (/volume1/repos/movies_backend).
4. Ejecutar el comando `docker build -t movies-backend .` para construir la imagen.
5. Desde DSM, arrancar un contenedor de la nueva imagen creada. Mapear el puerto local 8000 al puerto 3001 del contenedor
para que el proxy inverso del NAS acceda al contenedor correctamente.
