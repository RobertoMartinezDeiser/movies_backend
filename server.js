const express = require('express'); // http://expressjs.com/en/api.html
var cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
var data = require('./data');
var pruebas = require('./pruebas');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();

// (optional) only made for logging and bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

// // this is our update method, this method overwrites existing data in our database
// router.post('/updateData', (req, res) => {
//     const { id, update } = req.body;
//     Data.findByIdAndUpdate(id, update, (err) => {
//         if (err) return res.json({ success: false, error: err });
//         return res.json({ success: true });
//     });
// });

// // this is our create methid, this method adds new data in our database
// router.post('/putData', (req, res) => {
//     let data = new Data();
//     const { id, message } = req.body;
//     if ((!id && id !== 0) || !message) {
//         return res.json({
//             success: false,
//             error: 'INVALID INPUTS',
//         });
//     }
//     data.message = message;
//     data.id = id;
//     data.save((err) => {
//         if (err) return res.json({ success: false, error: err });
//         return res.json({ success: true });
//     });
// });

router.get('/getHello', (req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World\n');
});

router.get('/getSimpleJson', (req, res) => {
    res.json({ "foo": "bar" });
});

router.get('/getPromesa', (req, res) => {
    // llamamos a una promesa sin parametros
    // data.suma().then(suma => res.json({ "ResultadoSuma": suma }));
    
    // Llamamos a una promesa con parametros
    // data.sumaDosNumeros(4, 2).then(suma => res.json({ "ResultadoSuma": suma }));

    // Llamamos a dos promesas y esperamos por ambas
    var promise1 = pruebas.suma();
    var promise2 = pruebas.sumaDosNumeros(5, 3);
    Promise.all([promise1, promise2]).then(valores => res.json({
        "Promesa1": valores[0],
        "Promesa2": valores[1],
    }));
});

//http://localhost:3001/api/getpeliculas
router.get('/getPeliculas', (req, res) => {
    data.getPeliculas(undefined, 'ORDER BY m.c00').then(rows => res.json({ success: true, data: rows }));
});

//http://localhost:3001/api/getpeliculas/momia
router.get('/getPeliculas/:titulo', (req, res) => {
    var titulo = req.params.titulo;
    data.getPeliculas(titulo, 'ORDER BY m.c00').then(rows => res.json({ success: true, data: rows }));
});

//http://localhost:3001/api/getPeliculasRecientes
router.get('/getPeliculasRecientes', (req, res) => {
    data.getPeliculas(undefined, 'ORDER BY f.dateAdded DESC', 'LIMIT 25').then(rows => res.json({ success: true, data: rows }));
});

//http://localhost:3001/api/getSeries
router.get('/getSeries', (req, res) => {
    data.getSeries().then(rows => res.json({ success: true, data: rows }));
});

//http://localhost:3001/api/getStats
router.get('/getStats', (req, res) => {
    var promise1 = data.getNumeroPeliculas();
    var promise2 = data.getNumeroPeliculasHd();
    Promise.all([promise1, promise2]).then(valores => res.json({
        'success': true,
        'total': valores[0],
        'hd': valores[1],
        'sd': valores[0] - valores[1],
    }));
});

// append /api for our http requests
app.use('/api', router);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));