const mariadb = require('mariadb');
const config = require('./config');

const pool = mariadb.createPool({
    host: config.host,
    database: config.database,
    user: config.user,
    password: config.password,
    connectionLimit: 5
});

module.exports = {
    getPeliculas: function (titulo, ordenacion = '', limite = '') {
        //https://mariadb.com/kb/en/library/connector-nodejs-promise-api/#poolgetconnection-promise
        return new Promise((resolve, reject) => {
            // console.log('titulo: ' + titulo);
            // console.log('ordenacion: ' + ordenacion);
            // console.log('limite: ' + limite);

            var sqlbase = "SELECT m.idMovie, m.c00 titulo, uid.value imdb, " +
                "case WHEN year(premiered) is null THEN premiered ELSE year(premiered) END year, " +
                "(select if (sd0.iVideoWidth >= 1280 or sd0.iVideoHeight >= 720, 'HD', 'SD') as calidad " +
                "from streamdetails sd0 where sd0.idFile = f.idFile and sd0.iStreamType = 0) calidad, " +
                "date_format(f.dateAdded, '%d-%m-%Y') dateAdded, group_concat(sd1.strAudioLanguage) audio " +
                "FROM movie m inner join files f on f.idFile = m.idFile " +
                "left join streamdetails sd1 on sd1.idFile = f.idFile and sd1.iStreamType = 1 " +
                "left join uniqueid uid on uid.uniqueid_id = m.c09 " +
                "WHERE m.c00 like ? " +
                "GROUP BY m.idMovie ";

            var sql = sqlbase + ordenacion + ' ' + limite;

            if (typeof titulo !== 'undefined') {
                titulo = '%' + titulo + '%';
            } else {
                titulo = '%';
            }

            // console.log('sql: ' + sql);
            // console.log('filtro titulo: ' + titulo);

            pool
                .query(sql, [titulo])
                .then(rows => {
                    resolve(rows);
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    getNumeroPeliculas: function () {
        return new Promise((resolve, reject) => {
            var sql = "select count(*) numero from movie";
            pool
                .query(sql)
                .then(rows => {
                    resolve(rows[0].numero);
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    getNumeroPeliculasHd: function () {
        return new Promise((resolve, reject) => {
            var sql = "select count(m.c00) numerohd from movie m inner join files f on f.idFile = m.idFile " +
                "left join streamdetails sd0 on sd0.idFile = f.idFile and sd0.iStreamType = 0 " +
                "where sd0.iVideoWidth >= 1280 or sd0.iVideoHeight >= 720";
            pool
                .query(sql)
                .then(rows => {
                    resolve(rows[0].numerohd);
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    getSeries: function () {
        return new Promise((resolve, reject) => {
            var sql = "select c00 titulo, date_format(c05, '%d-%m-%Y') fechaemision, c12 tvid from tvshow WHERE c00 like ? ORDER BY c00 "
            pool
                .query(sql, ['%'])
                .then(rows => {
                    resolve(rows);
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
};