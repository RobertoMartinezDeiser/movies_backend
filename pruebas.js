module.exports = {
    suma: function () {
        return new Promise ((resolve, reject) => {
            var resultado = 2 + 4;
            resolve(resultado);
        });
    },
    sumaDosNumeros: function (n1, n2) {
        return new Promise ((resolve, reject) => {
            var resultado = n1 + n2;
            resolve(resultado);
        });
    },
}